
public class RunState {

	public static void main(String[] args) {
		Semaforo semaforo = new Semaforo();
		semaforo.mostrar();
		
		semaforo.setState(new EstadoAmarillo());
		semaforo.mostrar();
		
		semaforo.setState(new EstadoRojo());
		semaforo.mostrar();
		
		//Output console 
		
		//Luz Verde avanzar
		//Luz Amarilla precaución
		//Luz Roja detener


	}

}
