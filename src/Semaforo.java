
public class Semaforo {
	
	
	private EstadoSemaforo actualEstado;
	
	public Semaforo() {
		
		this.actualEstado = new EstadoVerde();
		
	}
	
	public void setState(EstadoSemaforo estado) {
		
		this.actualEstado = estado;
	}
	
	public void mostrar() {
		
		actualEstado.mostrar();
		
	}
}
